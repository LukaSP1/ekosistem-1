function [A,b,X] = generate(coefA,coefB)
%Funkcija za generiranje nakljucnega ekosistema s tremi plenilci, dvema
%rastlinojedoma in rastlinami
    
    mask = [ 
             0, 1, 1, 0, 0, 0;
            -1, 0, 0, 1, 0, 0;
            -1, 0, 0, 1, 1, 0;
             0,-1,-1, 0, 0, 1;
             0, 0,-1, 0, 0, 1;
             0, 0, 0,-1,-1, 0
           ];

    while 1
        % generiramo 6x6 random matriko z masko mask
        A = rand(6).*mask*coefA;

        % generiramo random 6x1 stolpicni vektor
        b = [-0.1 -0.1 -0.1 -0.1 -0.1 0.1]'.* rand(6,1)*coefB;

        % iscemo samo pozitivne stacionarne tocke
        x0 = A\-b;
        if ~sum(x0<0) && Jacobi(A, b, x0)
            break;
        end
    end
    
    X = x0;
end


% Jacobian
% izracuna Jacobijevo matriko sistema, in vrne nazaj 0 ali 1,0 ce je sistem
% asimptoticno unstable in 1 ce je sistem asimptoticno stable
function [isStable] = Jacobi(A, b, X)
    J=zeros(6);
    isStable = 0;
    
    % racunanje Jacobijeve matrike
    for i = 1:6
        for j = 1:6
            J(i, j) = X(i)*A(i, j);
        end
    end
    J = J - diag(diag(J)) + diag(A*X+b);
    
    % preverjanje ali je sistem stable
%     if  ~sum(real(eig(J)) > 0) % if stabilen
    if all(real(eig(J)) < 1e-19 & real(eig(J)) > -1e-19 ) && sum(imag(eig(J)) > 0) % if ciklicen
        isStable = 1;
    end
end



% Primer A b in X za testiranje
% A = [0 0.5 0.7 0 0 0;
% -2 0 0 1 0 0;
% -8 0 0 1 4 0;
% 0 -6 -1 0 0 2;
% 0 0 -1 0 0 1;
% 0 0 0 -2 -5 0]*0.001;
% 
% b = [-0.1, -0.1, -0.1, -0.05, -0.05, 0.3]';
%
%Ta X vrne periodicno obnasanje:
% X =  [9.0, 32.6, 125.9, 121.0, 16.5, 175.4]';
